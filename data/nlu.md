## intent:greet
- Hi
- Hey
- Hello
- Good morning
- hi again
- hi there
- greetings
- yo
- hola

## intent:show_user_tweets
- show me tweets of [tcsindia](user)
- get me tweets of [tcsus](user)
- lets see [tcs](user) tweets
- lets have tweets from [tcs](user)
- show tweets by [tcsin](user)
- show [tcs](user) tweets
- find tweets by [tcsindia](user)
- show me tweets of [tcsindia](user) from [21-10-2019](from) to [22-11-2019](to)

## intent:show_hashtag_tweets
- show tweets with hashtag [banana](hashtag)
- get me data of hashtag [tcsus](hashtag)
- lets see tweets with hashtag [tcsus](hashtag)
- lets have tweets of hashtag [tcs](hashtag)
- show tweets having hashtag [tcsin](hashtag)
- get tweets with [tcs](hashtag) hashtag
- find tweets having hashtag [tcsindia](hashtag)

## intent:thankyou
- um thank you good bye
- okay thank you good bye
- you rock
- thank you and good bye
- thank you goodbye noise
- okay thank you goodbye
- thank you goodbye
- okay thank you
